﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CSC160.FileExplorer.Backend;
using CSC160.FileExplorer.WPF.Controls;
using CSC160.FileExplorer.WPF.FileTreeView;
using System.Collections.ObjectModel;
using System.Threading;

namespace CSC160.FileExplorer.WPF
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		/*
		PERSONAL FEATURE: Adrian Ordonez
		File Explorer Icons are selectable by the keyboard and can be opened by
		a double mouse click or pressing Enter. Holding down shift will allow for
		multi select by the keyboard and holding down CTRL will allow for multi
		select through the mouse.
		Learned about the difficulties of working with a wrap panel and if were doing
		it again, I would've used a virtualized listview and customized the layout to 
		switch from list to wrap.
		*/

		#region Constructor(s)
		public MainWindow()
		{
			InitializeComponent();
		}
		#endregion

		#region Field
		private int selectedItemIndex;
		private Direction selectionDirection = Direction.NEUTRAL;
		private DirectoryNode currentDirectory;
		#endregion

		#region Methods
		private void showInFileExplorer(DirectoryNode dir, sortState sortby = sortState.NAME)
		{
			fileExplorer.Children.Clear();
			IList<FileExplorerIcon> items = new List<FileExplorerIcon>();
			selectedItemIndex = 0;
			try
			{
				if (dir?.Children != null)
					foreach (var item in dir?.Children)
					{
						FileExplorerIcon icon = new FileExplorerIcon(item);
						items.Add(icon);
						initializeFileIconEvents(item, icon);
					}
			}
			catch (AccessViolationException e)
			{

			}
			if (sortby == sortState.NAME)
			{

				items = items.OrderBy(e => e.FileName).ToList();
			}
			else
			{
				items = items.OrderBy(e => e.Size).ToList();
			}
			foreach (FileExplorerIcon fileExplorerIcon in items)
			{
				fileExplorer.Children.Add(fileExplorerIcon);
			}
			currentDirectory = dir;
		}

		private void refreshDirectory()
		{
			showInFileExplorer(currentDirectory);
		}

		private void initializeFileIconEvents(TreeNode item, FileExplorerIcon icon)
		{
			try
			{
				icon.MouseDown += delegate
				{
					if (!Keyboard.IsKeyDown(Key.LeftCtrl) && !Keyboard.IsKeyDown(Key.RightCtrl))
					{
						foreach (var child in fileExplorer.Children)
						{
							FileExplorerIcon deselect = child as FileExplorerIcon;
							if (deselect != null) deselect.IsSelected = false;
						}
					}
					icon.IsSelected = true;
					selectedItemIndex = fileExplorer.Children.IndexOf(icon);
				};
				if ((item as DirectoryNode) != null)
				{

					icon.MouseDoubleClick += delegate
					{
						showInFileExplorer(item as DirectoryNode);
					};
				}
				if ((item as FileNode) != null)
				{
					icon.MouseDoubleClick +=
					delegate
					{
						System.Diagnostics.Process.Start(icon.Path);

					};
				}
			}
			catch (AccessViolationException e)
			{

			}
		}

        /*
            PERSONAL FEATURE: Gavin Ortiz
            This method is called inside a text changed event attached to the textbox used for setting the
            root directory of the treeview. It updates the root as the user types in or changes the text in
            the box.
        */
		private void ChangeRoot(string path)
		{
			try
			{
				fileTreeView.Items.Clear();
				DirectoryInfo dir = new DirectoryInfo(path);
				if (!dir.Exists) return;
				DirectoryNode root = new DirectoryNode(dir)
				{
					Directory = dir,
					PathName = dir.Name,
					PathFullName = dir.FullName
				};

				fileTreeView.Items.Add(root);
			}
			catch
			{

			}
		}

		private void selectAll(bool AllSelected)
		{
			foreach (var child in fileExplorer.Children)
			{
				FileExplorerIcon selection = child as FileExplorerIcon;
				if (selection != null) selection.IsSelected = AllSelected;
			}
		}

		private bool checkSingleSelection()
		{
			int numberOfSelection = 0;
			foreach (FileExplorerIcon child in fileExplorer.Children)
			{
				if (child.IsSelected)
				{
					numberOfSelection++;
				}
			}

			return numberOfSelection == 1;
		}

		private void selectRange(int indexA, int indexB)
		{

			selectedItemIndex = indexB;
			if (indexA < indexB)
			{
				if (selectionDirection == Direction.NEUTRAL || selectionDirection == Direction.RIGHT)
					indexA++;
				if (selectionDirection == Direction.NEUTRAL)
					selectionDirection = Direction.RIGHT;
				while (indexA <= indexB)
				{

					FileExplorerIcon file = (fileExplorer.Children[indexA] as FileExplorerIcon);
					if (file.IsSelected && !Equals(file, fileExplorer.Children[indexB]))
						file.IsSelected = false;
					else
						file.IsSelected = true;
					indexA++;
				}
			}
			else if (indexA > indexB)
			{
				if (selectionDirection == Direction.NEUTRAL || selectionDirection == Direction.LEFT)
					indexA--;
				if (selectionDirection == Direction.NEUTRAL)
					selectionDirection = Direction.LEFT;
				while (indexA >= indexB)
				{

					FileExplorerIcon file = (fileExplorer.Children[indexA] as FileExplorerIcon);
					if (file.IsSelected && !Equals(file, fileExplorer.Children[indexB]))
						file.IsSelected = false;
					else
						file.IsSelected = true;
					indexA--;
				}
			}
		}
		#endregion

		#region	Events
		private void openDirectory(object sender, RoutedEventArgs e)
		{
			System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
			System.Windows.Forms.DialogResult result = dialog.ShowDialog();

			pathField.Text = dialog.SelectedPath;
		}

		private void selectedItemChangeListener(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			DirectoryNode directoryNode = fileTreeView.SelectedItem as DirectoryNode;
			if (directoryNode != null)
			{
				showInFileExplorer(directoryNode);
			}
			else
			{
				FileNode fileNode = fileTreeView.SelectedItem as FileNode;
				showInFileExplorer((fileNode?.Parent as DirectoryNode));
			}
		}


		private void KeyBindings(object sender, KeyEventArgs e)
		{
			if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
			{
				if (Keyboard.IsKeyDown(Key.A))
				{
					selectAll(true);
				}
				if (Keyboard.IsKeyDown(Key.D))
				{
					selectAll(false);
				}
			}
		}


		private void PreviewKeyBindings(object sender, KeyEventArgs e)
		{
			if (Keyboard.IsKeyUp(Key.LeftShift) && Keyboard.IsKeyUp(Key.RightShift))
			{
				selectionDirection = Direction.NEUTRAL;
			}

			if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
			{
				if (Keyboard.IsKeyDown(Key.Left) && selectionDirection == Direction.NEUTRAL)
				{
					selectionDirection = Direction.LEFT;
				}
				if (Keyboard.IsKeyDown(Key.Right) && selectionDirection == Direction.NEUTRAL)
				{
					selectionDirection = Direction.RIGHT;
				}
			}


			if (Keyboard.IsKeyDown(Key.Down))
			{

				int rowCount = (int)(fileExplorer.ActualWidth / 76);
				int indexTo = selectedItemIndex + rowCount;
				if ((Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)) && indexTo < fileExplorer.Children.Count)
					selectRange(selectedItemIndex, indexTo);
				else if ((Keyboard.IsKeyUp(Key.LeftShift) || Keyboard.IsKeyUp(Key.RightShift)) && indexTo < fileExplorer.Children.Count)
				{
					selectedItemIndex = indexTo;
					selectAll(false);
					(fileExplorer.Children[indexTo] as FileExplorerIcon).IsSelected = true;
				}
			}
			if (Keyboard.IsKeyDown(Key.Up))
			{
				int rowCount = (int)(fileExplorer.ActualWidth / 76);
				int indexTo = selectedItemIndex - rowCount;
				if ((Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)) && indexTo >= 0)
					selectRange(selectedItemIndex, indexTo);
				else if ((Keyboard.IsKeyUp(Key.LeftShift) || Keyboard.IsKeyUp(Key.RightShift)) && indexTo >= 0)
				{
					selectedItemIndex = indexTo;
					selectAll(false);
					(fileExplorer.Children[indexTo] as FileExplorerIcon).IsSelected = true;
				}
			}

			if (Keyboard.IsKeyDown(Key.Left))
			{
				if (selectedItemIndex != 0)
				{
					if (selectionDirection != Direction.LEFT)
					{
						(fileExplorer.Children[selectedItemIndex] as FileExplorerIcon).IsSelected = false;
					}
					if (selectionDirection == Direction.NEUTRAL)
					{
						selectAll(false);
					}
						(fileExplorer.Children[selectedItemIndex - 1] as FileExplorerIcon).IsSelected = true;
					selectedItemIndex = selectedItemIndex - 1;
				}
			}
			if (Keyboard.IsKeyDown(Key.Right))
			{
				if (selectedItemIndex < fileExplorer.Children.Count - 1)
				{
					if (selectionDirection != Direction.RIGHT)
					{
						(fileExplorer.Children[selectedItemIndex] as FileExplorerIcon).IsSelected = false;
					}
					if (selectionDirection == Direction.NEUTRAL)
					{
						selectAll(false);
					}
					(fileExplorer.Children[selectedItemIndex + 1] as FileExplorerIcon).IsSelected = true;
					selectedItemIndex = selectedItemIndex + 1;
				}
			}
			if (checkSingleSelection())
			{
				selectionDirection = Direction.NEUTRAL;

				if (Keyboard.IsKeyDown(Key.Enter))
				{
					FileExplorerIcon file = fileExplorer.Children[selectedItemIndex] as FileExplorerIcon;
					TreeNode selected = file.Node as TreeNode;
					if ((selected as DirectoryNode) != null)
					{
						showInFileExplorer(selected as DirectoryNode);


					}
					if ((selected as FileNode) != null)
					{
						System.Diagnostics.Process.Start(file.Path);
					}
				}

			}
		}


		private void pathField_TextChanged(object sender, TextChangedEventArgs e)
		{
			ChangeRoot(pathField.Text);
		}

		private void sortByName(object sender, RoutedEventArgs e)
		{
			showInFileExplorer(currentDirectory, sortState.NAME);
		}

		private void sortBySize(object sender, RoutedEventArgs e)
		{
			showInFileExplorer(currentDirectory, sortState.SIZE);
		}
		#endregion

		#region Enum(s)
		private enum Direction
		{
			LEFT,
			RIGHT,
			NEUTRAL
		}
		private enum sortState
		{
			NAME,
			SIZE
		}
		#endregion


	}


}

