﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace CSC160.FileExplorer.WPF
{
	class StringNotEmptyValidator : ValidationRule
	{
		public override ValidationResult Validate(object value, CultureInfo cultureInfo)
		{
			string input = value as string;
			if (input?.Replace(" ", "") == "")
			{
				return new ValidationResult(false,$"File name cannot be empty");
			}
			return new ValidationResult(true,null);
		}
	}
}
