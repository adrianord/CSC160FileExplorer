﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.VisualStyles;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using CSC160.FileExplorer.WPF.FileTreeView;
using System.Windows.Input;
using System.Diagnostics;
using System.Collections.Specialized;

namespace CSC160.FileExplorer.WPF.Controls
{
    /*
	PERSONAL FEATURE: Adrian Ordonez
	Control template code behind for individual file explorer icons.
	Learned about making control templates and dependency properties,
	as well as making events.
	*/

    /* PERSONAL FEATURE: Dallin Boyce 
I made a context menu for the right click on the files. 
I learned that you can use CommandBindings as an event. There is no other way I know of making a method run on one without it.
I really do not like how the methods and RoutedCommands must be static. There must be some reason in the underlying code that
I'm not sure about.
I also realized in other programs how many native calls people make and why there are more applications made on windnows and not for mac and linux.
The switching cost of making the same application on another OS would be horrendous especially with all of the native calls we make on this.
What happens:
    I made 3 Options that work as of right now.
    1. Open File -> You can right click on the files in the explorer and open them. That part was easy.
    2. Show In Explorer -> You can right click on the files in the explorer and we open the windows explorer in the parent folder of where the item is saved.
    3. Copy -> when selected on an file in the explorer you can right click and copy any file to your clipboard and paste them anywhere within windows explorer.
*/
    class FileExplorerIcon : Control
	{

		#region Constructors
		public FileExplorerIcon(TreeNode node)
		{
			FileName = node.PathName;
			Node = node;
			Size = node.SizeString;
			Path = node.PathFullName;
			setIcon(node.PathFullName);
		}

		static FileExplorerIcon()
		{
			Type owner = typeof(FileExplorerIcon);
			DefaultStyleKeyProperty.OverrideMetadata(owner, new FrameworkPropertyMetadata(owner));
			CommandBinding showInExplorerBind = new CommandBinding(showInExplorer, showInExplorerWindows, (Sender, e) => e.CanExecute = true);
			CommandBinding openBind = new CommandBinding(open, openFile, (Sender, e) => e.CanExecute = true);
			CommandBinding copyBind = new CommandBinding(copy, copyFile, (Sender, e) => e.CanExecute = true);
			CommandManager.RegisterClassCommandBinding(typeof(FileExplorerIcon), showInExplorerBind);
			CommandManager.RegisterClassCommandBinding(typeof(FileExplorerIcon), openBind);
			CommandManager.RegisterClassCommandBinding(typeof(FileExplorerIcon), copyBind);

		}
		#endregion

		#region Commands
		public static RoutedCommand showInExplorer { get; } = new RoutedCommand("Show", typeof(FileExplorerIcon));
		public static RoutedCommand open { get; } = new RoutedCommand("Open", typeof(FileExplorerIcon));
		public static RoutedCommand copy { get; } = new RoutedCommand("Copy", typeof(FileExplorerIcon));



		private static void showInExplorerWindows(Object sender, ExecutedRoutedEventArgs e)
		{
			FileExplorerIcon icon = (FileExplorerIcon)sender;
			Process.Start("explorer.exe", "/select," + icon.Path);
		}

		private static void openFile(Object sender, ExecutedRoutedEventArgs e)
		{
			FileExplorerIcon icon = (FileExplorerIcon)sender;
			Process.Start(@icon.Path);
		}

		private static void copyFile(Object sender, ExecutedRoutedEventArgs e)
		{
			StringCollection paths = new StringCollection();
			FileExplorerIcon icon = (FileExplorerIcon)sender;
			paths.Add(icon.Path);
			Clipboard.SetFileDropList(paths);
		}
		#endregion

		#region Methods
		private void setIcon(string path)
		{

			SHFILEINFO shinfo = new SHFILEINFO();
			IntPtr hImageLarge = Win32.SHGetFileInfo(
			path, 0, ref shinfo, (uint)Marshal.SizeOf(shinfo), Win32.SHGFI_ICON | Win32.SHGFI_LARGEICON);
			using (Icon i = System.Drawing.Icon.FromHandle(shinfo.hIcon))
			{
				ImageSource img = Imaging.CreateBitmapSourceFromHIcon(
					i.Handle,
					new Int32Rect(0, 0, i.Width, i.Height),
					BitmapSizeOptions.FromEmptyOptions());
				Icon = img;
			}


		}
		#endregion

		#region Fields
		public TreeNode Node { get; }
		#endregion

		#region Events

		public delegate void selectedEventHandler(object sender, EventArgs e);

		public event selectedEventHandler selected;
		#endregion

		#region Properties
		public string FileName
		{
			get { return (string)GetValue(FileNameProperty); }
			set { SetValue(FileNameProperty, value); }
		}

		public string Path { get; set; }

		public string Size { get; set; }

		public bool IsSelected
		{
			get { return (bool)GetValue(IsSelectedProperty); }
			set
			{
				SetValue(IsSelectedProperty, value);
				if (value)
				{
					selected?.Invoke(this, EventArgs.Empty);
				}
			}
		}

		public ImageSource Icon
		{
			get { return (ImageSource)GetValue(IconProperty); }
			set { SetValue(IconProperty, value); }
		}

		#region Dependency Properties
		public static readonly DependencyProperty FileNameProperty = DependencyProperty.Register(
			"FileName", typeof(string), typeof(FileExplorerIcon), new FrameworkPropertyMetadata());

		public static readonly DependencyProperty IconProperty = DependencyProperty.Register(
			"Icon", typeof(ImageSource), typeof(FileExplorerIcon), new FrameworkPropertyMetadata());

		public static readonly DependencyProperty IsSelectedProperty = DependencyProperty.Register(
			"IsSelected", typeof(bool), typeof(FileExplorerIcon), new FrameworkPropertyMetadata());
		#endregion
		#endregion

	}
}
