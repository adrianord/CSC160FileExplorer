﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

/*
    Author: Gavin Ortiz
*/

namespace CSC160.FileExplorer.WPF.FileTreeView
{
    public abstract class TreeNode
    {
        public TreeNode Parent { get; set; }
        public string PathName { get; set; }
		public string PathFullName { get; set; }
        public long Size
        {
            get
            {
                return GetSize();
            }
        }
        public string SizeString
        {
            get
            {
                return Backend.FileExplorer.FileSizeToString(Size);
            }
        }
        public int SizePercentage
        {
            get
            {
                return Parent != null ? (int)(Size / Parent.Size)/100 : 100;
            }
        }

        public DriveInfo RootDrive
        {
            get
            {
                return GetDrive();
            }
        }
        public DateTime CreationDate { get; set; }
        public TimeSpan CreationTime { get; set; }


        public abstract long GetSize();
        public abstract DriveInfo GetDrive();
    }
}
