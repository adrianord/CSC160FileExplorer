﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

/*
    Author: Gavin Ortiz
*/

namespace CSC160.FileExplorer.WPF.FileTreeView
{
    public class DirectoryNode : TreeNode
    {
        public DirectoryInfo Directory { get; set; }
        public IEnumerable<TreeNode> Children
        {
            get
            {
                IList<TreeNode> children = new List<TreeNode>();

                try
                {
                    foreach (var subDir in Directory.EnumerateDirectories("*"))
                    {
                        children.Add(new DirectoryNode(subDir) {
                            PathName = subDir.Name,
                            PathFullName = subDir.FullName,
                            CreationDate = subDir.CreationTime.Date,
                            CreationTime = subDir.CreationTime.TimeOfDay,
                            Parent = this
                        });
                    }
                }
                catch
                {

                }

                try
                {
                    foreach (var file in Directory.EnumerateFiles("*.*"))
                    {
                        children.Add(new FileNode(file)
                        {
                            PathName = file.Name,
                            PathFullName = file.FullName,
                            CreationDate = file.CreationTime.Date,
                            CreationTime = file.CreationTime.TimeOfDay,
                            Parent = this
                        });
                    }
                }
                catch
                {

                }

                return children;
            }
        }
        
        public DirectoryNode(DirectoryInfo dir)
        {
            Directory = dir;
        }

        public override long GetSize()
        {
            return Backend.FileExplorer.GetDirectorySize(Directory); ;
        }

        public override DriveInfo GetDrive()
        {
            return new DriveInfo(Directory.Root.Name);
        }
    }
}
