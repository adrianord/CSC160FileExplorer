﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
    Author: Gavin Ortiz
*/

namespace CSC160.FileExplorer.WPF.FileTreeView
{
    public class FileNode : TreeNode
    {
        public FileInfo File { get; set; }

        public FileNode(FileInfo file)
        {
            File = file;
        }

        public override long GetSize()
        {
            return Backend.FileExplorer.GetFileSize(File);
        }

        public override DriveInfo GetDrive()
        {
            return new DriveInfo(Path.GetPathRoot(File.FullName));
        }
    }
}
