﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC160.FileExplorer.Backend
{
    public class FileTraverser<T>
    {
		public async static void TraverseAsync(DirectoryInfo dir, Func<DirectoryInfo, T, Task<T>> dFunc, T parent)
		{
			foreach (DirectoryInfo item in dir.EnumerateDirectories())
			{
				T newParent = await dFunc(item, parent);
				TraverseAsync(item, dFunc, newParent);
			}
		}

		public async static Task<string> DirSizeAsync(DirectoryInfo item)
		{
			return await Task.Factory.StartNew(() =>
			{
				double rawSize = item.EnumerateFiles("*.*", SearchOption.AllDirectories).Sum(fi => fi.Length);
				double size = rawSize;
				int current = 0;
				while (size > 1024)
				{
					current++;
					size = rawSize / Math.Pow(2, current * 10);
				}
				return $"{Math.Round(size, 2)} {(Sizes)current}";
			});
		}

		public enum Sizes
		{
			Bytes,
			KB,
			MB,
			GB,
			TB,
			PB
		}
	}
}
