﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC160.FileExplorer.Backend
{

    /* PERSONAL FEATURE: Dallin Boyce 
    I made class that retrieves file metdata and creates and custom object of properties and returns it.
*/

    public class FileMetadataRetriever
    {

        public FileMetadataRetriever()
        {

        }

        public FileMetadata getMetaData(String filePath)
        {
            FileInfo info = new FileInfo(filePath);
            FileMetadata metadata = new FileMetadata(info);
            return metadata;
        }
    }
}
