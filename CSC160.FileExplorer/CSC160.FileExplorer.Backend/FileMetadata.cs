﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC160.FileExplorer.Backend
{
    public class FileMetadata
    {

        private FileInfo info;
        public String Name { get; }
        public String Extension { get; }
        public long size { get; }
        public DateTime TimeCreated { get; }
        public DateTime LastModified { get; }
        public DateTime LastOpened { get; }
        public bool isReadOnly { get; }
        public String ContainingDirectoryPath { get; }
        public String FullPath { get; }
        public DirectoryInfo DirectoryInfo { get; }
        public FileInfo RawFileInfo { get; }

        public FileMetadata(FileInfo info)
        {
            this.info = info;
            this.Name = info.Name;
            this.size = info.Length;
            this.TimeCreated = info.CreationTime;
            this.LastModified = info.LastWriteTime;
            this.LastOpened = info.LastAccessTime;
            this.isReadOnly = info.IsReadOnly;
            this.ContainingDirectoryPath = info.DirectoryName;
            this.DirectoryInfo = info.Directory;
            this.Extension = info.Extension;
            this.FullPath = info.FullName;
        }

    }
}
