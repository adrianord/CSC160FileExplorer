﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

/*
    Author: Gavin Ortiz
*/

namespace CSC160.FileExplorer.Backend
{
    public enum FileSize
    {
        bytes,
        KB,
        MB,
        GB,
        TB
    }
    
    public class FileExplorer
    {
        public static string FileSizeToString(long size)
        {
            int sizeType = 0;
            string sizeStr = "";

            while (size >= 1024)
            {
                size /= 1024;
                sizeType++;
            }

            sizeStr += Enum.GetName(typeof(FileSize), sizeType);
            return string.Format("({0} {1})", size, sizeStr);
        }

        public static long GetFileSize(FileInfo file)
        {
            return file.Length;
        }

        public static long GetDirectorySize(DirectoryInfo dir)
        {
            long size = 0;
            
            try
            { 
                foreach(var fileInfo in dir.EnumerateFiles("*.*", SearchOption.AllDirectories))
                {
                    size += fileInfo.Length;
                }
            }
            catch (UnauthorizedAccessException UnAuthFile)
            {

            }

            return size;
        }
    }
}
